<header id="header" class="site-header header-style-3">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col col-md-3 col-md-offset-2">
                    <div class="social">
                        <span>Follow us: </span>
                        <ul>
                            <li><a href="https://www.facebook.com/PPJFKemendesa"><i class="ti-facebook"></i></a></li>
                            <li><a href="https://www.twitter.com/ppjf_kemendesa"><i class="ti-twitter-alt"></i></a></li>
                            <li><a href="https://www.instagram.com/ppjf_kemendesa/"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </div> <!-- end topbar -->

    <nav class="navigation navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="open-btn">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?= base_url() ?>/assets/logo/logoatashome.png" alt></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar-left navigation-holder">
                <button class="close-navbar"><i class="ti-close"></i></button>
                <ul class="nav navbar-nav">
                    <li class="<?= (uri_string() == '/') ? 'current-menu-parent' : '' ?>"><a href="<?=base_url()?>">Home</a></li>
                    
                    <li class="<?= (uri_string() == 'profile') ? 'current-menu-parent' : '' ?>"><a href="<?=base_url('profile')?>">Profile</a></li>
                    <li class="<?= (uri_string() == 'layanan') ? 'current-menu-parent' : '' ?>"><a href="<?=base_url('layanan')?>">Layanan</a></li>
                    <li class="<?= (uri_string() == 'pusat-data') ? 'current-menu-parent' : '' ?>"><a href="<?=base_url('pusat-data')?>">Data & informasi</a></li>
                    <li class="<?= (uri_string() == 'contact') ? 'current-menu-item' : '' ?>"  ><a href="<?=base_url('contact')?>">Kontak</a></li>
                </ul>
            </div><!-- end of nav-collapse -->

            <div class="cart-search-contact">
                <div class="header-search-form-wrapper">
                    <button class="search-toggle-btn"><i class="fi flaticon-search"></i></button>
                    <div class="header-search-form">
                        <form>
                            <div>
                                <input type="text" class="form-control" placeholder="Search here...">
                                <button type="submit"><i class="ti-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="donate-btn">
                    <a href="/auth" class="theme-btn-s1"> Admin</a>
                </div>
            </div>
        </div><!-- end of container -->
    </nav>
</header>