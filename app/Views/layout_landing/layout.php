<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?=isset($author) ? $author : "PPJF"?>">
    <meta name="description" content="<?=isset($title) ? $title : "PPJF"?>">

    <title> <?=isset($title) ? $title : "PPJF"?> </title>
    
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/themify-icons.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/flaticon.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/animate.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/owl.theme.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/slick.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/slick-theme.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/swiper.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/owl.transitions.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/odometer-theme-default.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/jquery.fancybox.css" rel="stylesheet">
    <link href="<?= base_url() ?>/assets_landing/css/style.css" rel="stylesheet">
    

    <link rel="icon" href="<?= base_url() ?>/assets/img/brand/logo5_1.png" type="image/png">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    

</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">
   
        <!-- start preloader -->
        <div class="preloader">
            <div class="preloader-inner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!-- end preloader -->

        <!-- Start header -->
        <?=$this->renderSection('header')?>
        <!-- end of header -->

        <!-- start content -->
        <?=$this->renderSection('content')?>

        <!-- start site-footer -->
        <?=$this->renderSection('footer')?>
        <!-- end site-footer -->
    </div>
    <!-- end of page-wrapper -->
    
    <!-- modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <!-- <h2>Modal Header</h2> -->
            </div>
            <div class="modal-body">
                <h1>Apakah anda PSM?</h1>
            </div>
            <div class="modal-footer">
                <a  href="https://cms-kemendesa.sketsahouse.com/member/login.php" target="_blank" class="btn btn-success">Ya</a> 
            </div>
        </div>

    </div>

    <!-- All JavaScript files
    ================================================== -->
    <script src="<?= base_url() ?>/assets_landing/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>/assets_landing/js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="<?= base_url() ?>/assets_landing/js/jquery-plugin-collection.js"></script>
    <script>
        // Get the modal
            var modal = document.getElementById("myModal");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            function show() {
                modal.style.display = "block";
            }

            show()

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
    </script>
    <!-- Custom script for this template -->
    <script src="<?= base_url() ?>/assets_landing/js/script.js"></script>
</body>
</html>
