<footer class="site-footer">
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-6">
                    <div class="widget about-widget">
                        <div class="widget-title">
                            <h3>
                                <span class="hidden">About us</span>
                                <img src="<?= base_url() ?>/assets/logo/logofooterhome.png" alt>
                            </h3>
                            <p>Follow Us</p>
                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a href="https://www.facebook.com/PPJFKemendesa"><i class="ti-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com/ppjf_kemendesa"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="https://www.instagram.com/ppjf_kemendesa/"><i class="ti-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-6">
                    <div class="widget contact-widget service-link-widget">
                        <div class="widget-title">
                            <h3>Lokasi kami</h3>
                        </div>
                        <ul>
                            <li>Jln.TMP kalibata no.17 jakarta selatan 12750</li>
                            <li><span>Tlpn:</span> 088212572692</li>
                            <li><span>Alamat Email:</span> ppjf.kemendesa@outlook.com</li>
                            <li><span>Jam Buka:</span> 08:00-17:00</li>
                        </ul>
                    </div>
                </div>
                
                
            </div>
        </div> <!-- end container -->
    </div>
    <div class="lower-footer">
        <div class="container">
            <div class="row">
                <div class="separator"></div>
                <div class="col col-xs-12">
                    <p class="copyright">Copyright &copy; 2021 PPJF BPSDM</p>
                    <div class="extra-link">
                        <ul>
                            <li><a href="#">Privace & Policy</a></li>
                            <li><a href="#">terms and conditions</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>