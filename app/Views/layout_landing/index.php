<?= $this->extend('layout_landing/layout') ?>
<?= $this->section('header') ?>
    <?= $this->include('layout_landing/header') ?>
<?= $this->endSection() ?>

<?= $this->section('footer') ?>
    <?= $this->include('layout_landing/footer') ?>
<?= $this->endSection() ?>