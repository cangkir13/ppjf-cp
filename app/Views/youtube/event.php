
<script>
    $(document).on("click", '.showDetail', function(e) {
        
        let dataid = $(this).attr('data-user')
        $.ajax({
            url : `<?=$route?>/${dataid}/get`,
            method: 'GET',
            success : function(result) {
                
                $(".modal-body #judul").text(result.title)
                $(".modal-body #isi").text(result.content)
                let yt = result.link
                $("#youtube").attr('src', yt)
                $("#youtube").attr('title', result.title);
            }
        })
    })

    $(document).on("click", '.deleteKOnten', function(e) {
        
        let dataid = $(this).attr('data-delete')
        $.ajax({
            url : `<?=$route?>/${dataid}/get`,
            method: 'GET',
            success : function(result) {
                $(".modal-footer #id_value").val(result.id)
                $(".body-delete #judul").text(result.title)
                $(".body-delete #isi").text(result.content.substr(0, 200, "..."))
                let img = result.link
                $(".body-delete #youtube").attr('src', img);
                // $("#youtube").attr('title', result.title);
            }
        })
    })
</script>