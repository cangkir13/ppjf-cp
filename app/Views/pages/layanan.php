<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
    <section class="page-title img-benner-layanan">
        <div class="page-title-container">
            <div class="page-title-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <h2>Layanan Kami</h2>
                            <ol class="breadcrumb">
                                <li><a href="<?=base_url()?>">Home</a></li>
                                <li>Layanan</li>
                            </ol>
                        </div>
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div>
        </div>
    </section>
    <!-- start causes-section -->
    <section class="causes-section causes-section-pg section-padding">
        <div class="container-fluid">
            <div class="section-title-s2">
                <span>#Aplikasi</span>
                <h2>Layanan  <span>Kami</span></h2>
            </div>
            <div class="content-area causes-slider">
                <div class="item">
                    <div class="inner">
                        <div class="img-holder">
                            <img src="assets/logo/coming_soon.jpg" alt>
                        </div>
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="#">Pendataan Pejabat Fungsional</a></h3>
                                <div class="goal-raised">
                                    <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At voluptatum.</span>
                                    <a href="#" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <img class="img-logo-org" src="assets/logo/e-dupak.png" alt>
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="https://cms-kemendesa.sketsahouse.com/">e-DUPAK </a></h3>
                                <div class="goal-raised">
                                    <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At voluptatum.</span>
                                    <a href="https://cms-kemendesa.sketsahouse.com/" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <img class="img-logo-org" src="<?=base_url()?>/assets/logo/isp.png" alt="isp">
                        
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="#"> </a></h3>
                                <div class="goal-raised">
                                    <span>Layanan Uji kompetensi ...</span>
                                    <a href="#" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                    <img class="img-logo-org" src="<?=base_url()?>/assets/logo/ipsmi.jpeg" alt="ipsmi">
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="#">Survei layanan</a></h3>
                                <div class="goal-raised">
                                    <span>Survei Layanan ...</span>
                                    <a href="#" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <div class="img-holder">
                            <img src="assets/logo/coming_soon.jpg" alt>
                        </div>
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="#">Konsultasi Pembinaan Jabatan Fungsional</a></h3>
                                <div class="goal-raised">
                                    <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At voluptatum.</span>
                                    <a href="#" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <img class="img-logo-org" src="<?=base_url()?>/assets/logo/ipsmi.jpeg" alt="ipsmi">
                        <div class="overlay">
                            <div class="overlay-content">
                                <h3><a href="#">Registrasi Organisasi Profesi</a></h3>
                                <div class="goal-raised">
                                    <span>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At voluptatum.</span>
                                    <a href="#" class="donate-btn"></i>Readmore..</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end causes-section -->
<?= $this->endSection() ?>