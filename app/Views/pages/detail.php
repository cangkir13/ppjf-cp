<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
    <!-- start page-title -->
    <section class="page-title img-benner-layanan">
        <div class="page-title-container">
            <div class="page-title-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <!-- <h2>Blog single</h2> -->
                            <ol class="breadcrumb">
                                <li><a href="/">Home</a></li>
                                <li>Article</li>
                            </ol>
                        </div>
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div>
        </div>
    </section>
    <!-- end page-title -->

    <!-- start blog-single-section -->
    <section class="blog-single-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-8">
                    <div class="blog-content">
                        <div class="post format-standard-image">
                            <div class="entry-media">
                                <img src="/<?=$article['image']?>" alt>
                            </div>
                            <div class="date-entry-meta">
                                <div class="entry-meta">
                                    <span>By: <a href="#"><?=$article['author']?></a> </span>
                                    <span>Date: <?=$article['created_at']?></span>
                                </div>
                            </div>
                            <h2><?=$article['title']?></h2>
                            <p>
                                <?=$article['content']?>
                            </p>
                        </div>

                    </div>                        
                </div>

                <div class="col col-md-4">
                    <div class="blog-sidebar">
                        
                        <div class="widget category-widget">
                            <h3>Categories</h3>
                            <ul>
                                <?php foreach($categoris as $categori) :?>
                                    <li><a href="<?=base_url('categories')."/".$categori['name']?>"><?=$categori['name']?> <span>(<?=$categori['jml']?>)</span></a></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <div class="widget tag-widget">
                            <h3>Tags</h3>
                            <ul>
                                <li><a href="#">Charity</a></li>
                                <li><a href="#">Organization</a></li>
                                <li><a href="#">Donation</a></li>
                                <li><a href="#">Manfacturing</a></li>
                                <li><a href="#">Children</a></li>
                                <li><a href="#">Funds</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
    <!-- end blog-single-section -->
<?= $this->endSection() ?>