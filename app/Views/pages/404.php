<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
<!-- start page-title -->
<section class="page-title">
    <div class="page-title-container">
        <div class="page-title-wrapper">
        </div>
    </div>
</section>
<!-- end page-title -->
<!-- start error-404-section -->
<section class="error-404-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <div class="content clearfix">
                    <div class="error">
                        <h2>404</h2>
                    </div>
                    <div class="error-message">
                        <h3>Oops! Page Not Found!</h3>
                        <p>We’re sorry but we can’t seem to find the page you requested. This might be because you have typed the web address incorrectly.</p>
                        <a href="#" class="theme-btn-s10">Back to home</a>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>    
<!-- end error-404-section --> 
<?= $this->endSection() ?>