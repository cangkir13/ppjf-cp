<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
<!-- start page-title -->
<section class="page-title img-benner-layanan">
    <div class="page-title-container">
        <div class="page-title-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Blog</h2>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li>Blog</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </div>
    </div>
</section>
<!-- end page-title -->

<!-- start blog-pg-section -->
<section class="blog-pg-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-md-10 col-md-offset-1">
                <div class="blog-content">
                    <?php foreach($articles as $value) :?>
                    <div class="post format-standard-image">
                        <div class="entry-media">
                            <img src="<?=base_url($value['image'])?>" alt>
                        </div>
                        <div class="entry-details">
                            <div class="date"><?=date('Y F d', strtotime($value['created_at']) ) ?></div>
                            <h3><a href="<?=base_url('/Article')."/".$value["title"]?>"><?=$value['title']?></a></h3>
                            <p><?=substr($value['content'], 0, 200) ?>...</p>
                            <a href="<?=base_url('/Article')."/".$value["title"]?>" class="read-more">Read More</a>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end blog-pg-section -->
<?= $this->endSection() ?>