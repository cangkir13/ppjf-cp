<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
    <!-- start page-title -->
    <section class="page-title img-benner-profile">
        <div class="page-title-container">
            <div class="page-title-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <!-- <h2>Tentang kami</h2>
                            <ul class="breadcrumb">
                                <li><a href="/">Home</a></li>
                                <li>Tentang kami</li>
                            </ul> -->
                        </div>
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div>
        </div>
    </section>
    <!-- end page-title -->
    
    <!-- start testimoninals-funfact-section -->
    <section class="testimoninals-funfact-section testimonials-pg-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="section-title-s3">
                    <span>#Tentang kami</span>
                        <h2>PUSAT PEMBINAAN	 <br>JABATAN FUNGSIONAL</h2>
                        <p>
                        BADAN PENGEMBANGAN SUMBER DAYA MANUSIA DAN PEMBERDAYAAN MASYARAKAT DESA, DAERAH TERTINGGAL DAN TRANSMIGRASI
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col col-xs-12">
                    <div class="testimonials-slider-s2 testimonials-slider-area">
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/kapus.jpeg" alt>
                                </div>
                                <h5>PUSAT PEMBINAAN JABATAN FUNGSIONAL</h5>
                                <span>PPJF</span>
                            </div>
                            <p>
                            Berdasarkan Peraturan Menteri Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi Republik Indonesia Nomor 16 Tahun 2020 Tentang Uraian Fungsi Organisasi Jabatan Pimpinan Tinggi Pratama Dan Tugas Kelompok Jabatan Fungsional Di Lingkungan Kementerian Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi
                            </p>
                            
                        </div>
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/tu.jpeg" alt>
                                </div>
                                <h5>KASUBAG TU</h5>
                                <span>PPJF</span>
                            </div>
                            <p>
                                <!-- <b>Kelompok substansi rencana dan program pengembangan jabatan fungsional</b>    -->
                                Berdasarkan Peraturan Menteri Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi Republik Indonesia Nomor 16 Tahun 2020 Tentang Uraian Fungsi Organisasi Jabatan Pimpinan Tinggi Pratama Dan Tugas Kelompok Jabatan Fungsional Di Lingkungan Kementerian Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi
                            </p>
                        </div>
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/kel1.jpeg" alt>
                                </div>
                                <h5>Kelompok 1</h5>
                                <span>PPJF</span>
                            </div>
                            <p>
                                <b>Kelompok substansi rencana dan program pengembangan jabatan fungsional</b>   
                                mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang penyusunan rencana dan program pengembangan jabatan fungsional yang berada dalam pembinaan teknis
                            </p>
                            
                        </div>
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/kel2.jpeg" alt>
                                </div>
                                <h5>Kelompok 2</h5>
                                <span>PPJF</span>
                            </div>
                            <p> <b>Kelompok substansi pengembangan standarisasi kompetensi</b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan standardisasi dan kompetensi jabatan fungsional yang berada dalam pembinaan teknis Kementerian.
                            </p>
                            
                        </div>
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/kel4.jpeg" alt>
                                </div>
                                <h5>Kelompok 3</h5>
                                <span>PPJF</span>
                            </div>
                            <p> <b>Kelompok substansi pengembangan profesi dan sertifikasi kelembagaan dan jabatan fungsional</b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan profesi dan sertifikasi kelembagaan dan jabatan fungsional yang berada dalam pembinaan teknis Kementerian
                            </p>
                            
                        </div>
                        <div class="grid">
                            <div class="author">
                                <div class="author-img">
                                    <img class="img-avatar-profile" src="assets/img/avatar/kel4.jpeg" alt>
                                </div>
                                <h5>Kelompok 4</h5>
                                <span>PPJF</span>
                            </div>
                            <p> <b>Kelompok substansi evaluasi dan penilaian jabatan fungsional </b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang evaluasi dan penilaian jabatan fungsional yang berada dalam pembinaan teknis Kementerian
                            </p>
                            
                        </div>
                    </div>
                </div> 
            </div>
        </div> <!-- end container -->
    </section>

     <!-- start mission-vision-section -->
     <section >
        <div class="container">
            <div class="row">
                <div class="col col-xs-6">
                    <div class="grid">
                        <div class="author">
                            <div class="author-img">
                                <img class="img-avatar-profile" src="assets/img/avatar/kel1.jpeg" alt>
                            </div>
                            <h5>Kelompok 1</h5>
                                <span>PPJF</span>
                                <p>
                                <b>Kelompok substansi rencana dan program pengembangan jabatan fungsional</b>   
                                mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang penyusunan rencana dan program pengembangan jabatan fungsional yang berada dalam pembinaan teknis
                            </p>
                            <br>
                            <dl>
                                <dt>a. sub kelompok substansi penyusunan rencana dan program</dt>
                                <ul>
                                    <li>mempunyai tugas melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang penyusunan rencana dan program pembinaan jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                                </ul>
                                <br>
                                <dt>b. sub kelompok substansi pengembangan jabatan fungsional.</dt>
                                <ul>
                                    <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan  teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                                </ul>
                            </dl>
                        </div>
                    </div>
                </div>   
                <div class="col col-xs-6">
                    <div class="grid">
                        <div class="author">
                            <div class="author-img">
                                <img class="img-avatar-profile" src="assets/img/avatar/kel2.jpeg" alt>
                            </div>
                            <h5>Kelompok 2</h5>
                            <span>PPJF</span>
                            <p> <b>Kelompok substansi pengembangan standarisasi kompetensi</b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan standardisasi dan kompetensi jabatan fungsional yang berada dalam pembinaan teknis Kementerian.
                            </p>
                            <br>
                            <dl>
                                <dt>a. sub kelompok substansi standardisasi profesi</dt>
                                <ul>
                                    <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang standardisasi profesi jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                                </ul>
                                
                                <dt>b. sub kelompok substansi sertifikasi</dt>
                                <ul>
                                    <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang sertifikasi jabatan fungsional yang berada dalam pembinaan teknis Kementerian.</li>
                                </ul>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-xs-6">
                    <div class="grid">
                        <div class="author">
                            <div class="author-img">
                                <img class="img-avatar-profile" src="assets/img/avatar/kel4.jpeg" alt>
                            </div>
                            <h5>Kelompok 3</h5>
                            <span>PPJF</span>
                        </div>
                        <p> <b>Kelompok substansi pengembangan profesi dan sertifikasi kelembagaan dan jabatan fungsional</b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan profesi dan sertifikasi kelembagaan dan jabatan fungsional yang berada dalam pembinaan teknis Kementerian
                        </p>
                        <br>
                        <dl>
                            <dt>a. sub kelompok substansi bimbingan profesi</dt>
                            <ul>
                                <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang bimbingan profesi jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                            </ul>
                            <br>
                            <dt>b. sub kelompok peningkatan kapasitas</dt>
                            <ul>
                                <li>melakukan pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang peningkatan kapasitas pejabat fungsional yang berada dalam pembinaan teknis Kementerian</li>
                            </ul>
                            <br>
                            <dt>c. sub kelompok substansi pengembangan jejaring kerja sama</dt>
                            <ul>
                                <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan jejaring kerja sama jabatan fungsional yang berada dalam pembinaan teknis Kementerian.</li>
                            </ul>
                        </dl>
                    </div>
                </div>
                <div class="col col-xs-6">
                    <div class="grid">
                        <div class="author">
                            <div class="author-img">
                                <img class="img-avatar-profile" src="assets/img/avatar/kel4.jpeg" alt>
                            </div>
                            <h5>Kelompok 4</h5>
                            <span>PPJF</span>
                        </div>
                        <p> <b>Kelompok substansi evaluasi dan penilaian jabatan fungsional </b>
                            mempunyai tugas melaksanakan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang evaluasi dan penilaian jabatan fungsional yang berada dalam pembinaan teknis Kementerian
                        </p>
                        <br>
                        <dl>
                            <dt>a. sub kelompok substansi evaluasi jabatan fungsional</dt>
                            <ul>
                                <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang evaluasi jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                            </ul>
                            <br>
                            <dt>b. sub kelompok penilaian pejabat fungsional</dt>
                            <ul>
                                <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang penilaian pejabat fungsional yang berada dalam pembinaan teknis Kementerian</li>
                            </ul>
                            <br>
                            <dt>c. sub kelompok substansi pengembangan sistem seleksi jabatan fungsional</dt>
                            <ul>
                                <li>melakukan pemberian pelayanan fungsional dalam penyusunan kebijakan teknis, pelaksanaan, serta evaluasi dan pelaporan di bidang pengembangan sistem seleksi jabatan fungsional yang berada dalam pembinaan teknis Kementerian</li>
                            </ul>
                        </dl>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>  
    <!-- end mission-vision-section -->   


<?= $this->endSection() ?>