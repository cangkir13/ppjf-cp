<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
<!-- start of hero -->
<section class="hero-slider hero-style-3">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            
            <?php foreach ($headline as $key => $value) :?>
            <div class="swiper-slide">
                <div class="slide-inner slide-bg-image" data-background="<?= base_url($value['image']) ?>">
                    <div class="container">
                        <div data-swiper-parallax="300" class="slide-title">
                            <h2><?=$value['title']?></h2>
                        </div>
                        <br>
                        <div data-swiper-parallax="400" class="slide-text">
                            <p><?php echo substr_replace($value['content'], "...", 200 )?></p>
                        </div>
                        <div class="clearfix"></div>
                        <div data-swiper-parallax="500" class="slide-btns">
                            <a href="<?=base_url('/Article')."/".$value["title"]?>" class="theme-btn-s2">Readmore</a> 
                        </div>
                    </div>
                </div> <!-- end slide-inner --> 
            </div> <!-- end swiper-slide -->
            <?php endforeach;?>    
            <!-- <div class="swiper-slide">
                <div class="slide-inner slide-bg-image" data-background="<?= base_url() ?>/assets_landing/images/slider/slide-4.jpg">
                    <div class="container">
                        <div data-swiper-parallax="300" class="slide-title">
                            <h2>Don't turn away, Give today!</h2>
                        </div>
                        <div data-swiper-parallax="400" class="slide-text">
                            <p>Magazine and housed in a nice, gilded frame. It showed a lady fitted out with hat and fur boa who sat upright, raising a heavy fur muff that</p>
                        </div>
                        <div class="clearfix"></div>
                        <div data-swiper-parallax="500" class="slide-btns">
                            <a href="#" class="theme-btn-s2">Donate Now <i class="fi flaticon-heart-1"></i></a> 
                        </div>
                    </div>
                </div> 
            </div>  -->
            <!-- end slide-inner --> 
            <!-- end swiper-slide -->
        </div>
        <!-- end swiper-wrapper -->

        <!-- swipper controls -->
        <div class="swiper-pagination"></div>
        <div class="pagi-arrow">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</section>
<!-- end of hero slider -->


<!-- start feature-section-s2 -->
<section class="feature-section-s2">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <div class="feature-grids clearfix">
                    <div class="grid">
                        <a href="#">
                            <div>
                                <img src="<?=base_url()?>/assets/logo/isp.png" width="100" alt="isp">
                            </div>
                            
                            <h3>UJI KOMPETENSI PSM</h3>
                            <div>
                                <p>
                                    Uji KOmpetensi PSM di selenggarakan bekerja sama dengan LSP-P2 KEMENDESA (Coming soon)
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="grid">
                        <a href="https://cms-kemendesa.sketsahouse.com/member/signup.php" target="_blank">
                            <!-- <i class="fi flaticon-pencil-case"></i> -->
                            <div>
                                <img src="/assets/logo/e-dupak.png" width="100" alt="edupak">
                            </div>
                            
                            <h3>e-DUPAK</h3>
                            <div>
                                <p>
                                    Aplikasi Penilaian angka kredit PSM 
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="grid">
                        <a href="#">
                            <!-- <i class="fi flaticon-coffee-cup"></i> -->
                            <div>
                                <img src="<?=base_url()?>/assets/logo/ipsmi.jpeg" width="100" alt="ipsmi">
                            </div>
                            <h3>IPSMI</h3>
                            <div>
                                <p>
                                    Organisasi Profesi Penggerak Swadaya Masyarakat yang disebut Ikatan Penggerak Swadaya Masyarakat Indonesia (Coming soon)
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end feature-section-s2 -->


<!-- start about-s3-section -->
<section class="about-s3-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-md-4">
                <div class="about-title-area">
                    <a href="/profile">
                        <div class="section-title">
                            <span>#Pusat Pembinaan Jabatan Fungsional</span>
                            <h2>Tentang Kami</h2>
                        </div>
                    </a>
                    <!-- <a href="#" class="theme-btn-s4">More about us</a> -->
                </div>
            </div>
            <div class="col col-md-4">
                <div class="img-holder">
                    <img src="<?= base_url() ?>/assets/logo/sipjflogoL2.png"  alt>
                </div>
            </div>
            <div class="col col-md-4">
                <div class="about-text-area">
                    <p>
                    Berdasarkan Peraturan Menteri Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi Republik Indonesia Nomor 16 Tahun 2020 Tentang Uraian Fungsi Organisasi Jabatan Pimpinan Tinggi Pratama Dan Tugas Kelompok Jabatan Fungsional Di Lingkungan Kementerian Desa, Pembangunan Daerah Tertinggal, Dan Transmigrasi
                    </p>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end about-s3-section -->



<!-- start events-section -->
<section class="events-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-md-5">
                <div class="section-title">
                    <span>#Pusat Pembinaan Jabatan Fungsional</span>
                    <h2>Kegiatan Kami</h2>
                </div>
                <!-- <div class="about-details">
                    <p>
                        PPJF ADALAH .....
                    </p>
                </div> -->
            </div>
            <div class="col col-md-7">

                <div class="events-slider-holder">
                    <div class="events-slider">
                        <?php foreach ($content as $key => $value) :?>
                            <div class="events-slider-item">
                                <div class="grid">
                                    <div class="img-holder">
                                        <img src="<?= base_url($value['image']) ?>" width="320" height="300" alt>
                                    </div>
                                    <div class="details">
                                        <div class="inner">
                                            <h3><a href="<?=base_url('/Article')."/".$value["title"]?>"><?=$value['title']?></a></h3>
                                            <p><?=$value['created_at']?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="events-slider-nav">
                        <div class="events-slider-arrows">
                            <div class="slider-prev"><i class="fi flaticon-back"></i></div>
                            <div class="slider-next"><i class="fi flaticon-next"></i></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end events-section -->
                        
<!-- start youtube -->

<section class="team-section section-padding">
    <div class="content-area">
        <div class="first-row clearfix">
            <div class="grid"></div>
            <div class="grid info-grid">
                <div class="section-title">
                    <span>#Pusat Pembinaan Jabatan Fungsional</span>
                    <h2>Dokumentasi</h2>
                </div>
            </div>
            <?php foreach ($youtube as $key => $value) :?>
            <div class="grid">
                <div class="img-holder">
                    <iframe src="<?= $value['link'] ?> " width="500" height="340" frameborder="0" allowfullscreen></iframe>
                </div>
                <h4><a href="#"><?=$value['title']?></a></h4>
                <p>Dedicated volunteer</p>
                
            </div>
            <?php endforeach;?>
        </div>
    </div> 
</section>
<!-- end youtube -->

        
<?= $this->endSection() ?>