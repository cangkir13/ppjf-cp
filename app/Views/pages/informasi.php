<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
    <!-- start page-title -->
    <section class="page-title img-benner-layanan">
        <div class="page-title-container">
            <div class="page-title-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <h2>Data dan Infromasi</h2>
                            <ol class="breadcrumb">
                                <li><a href="<?=base_url()?>">Home</a></li>
                                <li>Data dan Infromasi</li>
                            </ol>
                        </div>
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div>
        </div>
    </section>
        <!-- end page-title -->
    
    <section class="blog-pg-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-12">
                    <div class="blog-content">
                        
                        <div class="post format-gallery">
                            <h3>Dokumentasi foto</h3>
                            <div class="entry-media post-slider">
                                <?php foreach($articles as $value) : ?>
                                    <img src="<?= base_url($value['image']) ?>" alt="<?=$value['title']?>" width="1200" height="683" >
                                <?php endforeach?>
                            </div>
                            <div class="entry-details">
                                <div class="date">
                                    <a href="https://www.instagram.com/ppjf_kemendesa/" target="_blank">
                                    Go To link...
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="post format-video">
                            <h3>Channel Youtube</h3>
                            <div class="entry-media video-holder">
                                <img src="<?= base_url($articles[0]['image']) ?>" alt>
                                <a href="<?=$youtube['link']?>" class="video-btn" data-type="iframe">
                                    <i class="fi flaticon-play-button"></i>
                                </a>
                            </div>
                            <div class="entry-details">
                                <div class="date">
                                    <a href="https://www.youtube.com/channel/UClnRvpnmnNzbtqJgZGLOZYQ" target="_blank">
                                    Go To link...
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="post format-standard">
                            <h3><a href="https://jdih.kemendesa.go.id/" target="_blank">Produk Hukum</a> </h3>
                            <?php foreach($produks as $value) :?>
                            <div class="entry-details">
                                <div class="date"><?=date('Y-F-d', strtotime($value['created_at'])) ?></div>
                                <h3><a href="<?=$value['link']?>" target="_blank" ><?=$value['title']?></a></h3>
                                <!-- <p>Collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with</p> -->
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
    <!-- start contact-section -->
    <section class="feature-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Sosial Media</h1>
                </div>
                <div class="col col-xs-12">
                    <div class="feature-grids">
                        <div class="grid">
                            <a href="https://www.instagram.com/ppjf_kemendesa" target="_blank">
                                <img class="img-sosmed" src="<?=base_url()?>/assets/logo/instagram.jpg" alt="isp">
                                <h3>Instagram</h3>
                            </a>
                        </div>
                        <div class="grid">
                            <a href="https://www.facebook.com/PPJFKemendesa" target="_blank" rel="noopener noreferrer">
                                <img class="img-sosmed" src="<?=base_url()?>/assets/logo/facebook.png" alt="isp">
                                <h3>Facebook</h3>
                            </a>
                        </div>
                        <div class="grid">
                            <a href="https://www.twitter.com/ppjf_kemendesa" target="_blank" rel="noopener noreferrer">
                                <img class="img-sosmed" src="<?=base_url()?>/assets/logo/twitter.jpg" alt="isp">
                                <h3>Twitter</h3>
                            </a>
                        </div>
                        <div class="grid">
                            <a href="https://www.youtube.com/channel/UClnRvpnmnNzbtqJgZGLOZYQ" target="_blank" rel="noopener noreferrer">
                                <img class="img-sosmed" src="<?=base_url()?>/assets/logo/youtube.png" alt="isp">
                                <h3>Youtube</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
    <!-- end feature-section -->

<?= $this->endSection() ?>