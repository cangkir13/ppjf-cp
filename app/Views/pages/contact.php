<?= $this->extend('layout_landing/index') ?>
<?= $this->section('content') ?>
    <!-- start page-title -->
    <section class="page-title img-benner-kontak">
        <div class="page-title-container">
            <div class="page-title-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <h2>Kontak kami</h2>
                            <ol class="breadcrumb">
                                <li><a href="<?=base_url()?>">Home</a></li>
                                <li>Kontak</li>
                            </ol>
                        </div>
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div>
        </div>
    </section>
        <!-- end page-title -->
    <!-- start contact-section -->
    <section class="contact-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="section-title-s3">
                        <span>#Hubungi kami</span>
                        <h2>Butuh bantuan lain?</h2>
                        <!-- <p>Raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather or then turned to look out.</p> -->
                    </div>
                </div>
                <div class="col col-xs-12">
                    <div class="contact-info-grids">
                        <div class="grid">
                            <i class="fi flaticon-house"></i>
                            <h4>Lokasi kami</h4>
                            <p>Jln.TMP kalibata no.17 jakarta selatan 12750</p>
                        </div>
                        <div class="grid">
                            <i class="fi flaticon-email"></i>
                            <h4>Alamat email</h4>
                            <p>ppjf.kemendesa@outlook.com <br> Email pusat</p>
                        </div>
                        <div class="grid">
                            <i class="fi flaticon-call"></i>
                            <h4>Telpon</h4>
                            <p>088212572692 <br>Hubungi kami whatsApps</p>
                        </div>
                        <div class="grid">
                            <i class="fi flaticon-alarm"></i>
                            <h4>Buka pada</h4>
                            <p>Senin - Jumat  <br> 08:00-17:00</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="contact-form-area">
                <div class="row">
                    <div class="col col-md-4">
                        <div class="contact-text">
                            <h3>Still have fun with us!</h3>
                            <p>Daftakan email anda untuk dapatkan permberitahuan dari kami</p>
                        </div>
                    </div>
                    <div class="col col-md-8">
                        <div class="contact-form">
                            <form method="post" class="contact-validation-active" id="contact-form-main">
                                <div>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name*">
                                </div>
                                <div>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email*">
                                </div>
                                <div>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone*">
                                </div>
                                <div>
                                    <select name="subject" class="form-control">
                                        <option disabled="disabled" selected>Contact subject</option>
                                        <option>Subject 1</option>
                                        <option>Subject 2</option>
                                        <option>Subject 3</option>
                                    </select>
                                </div>
                                <div class="fullwidth">
                                    <textarea class="form-control" name="note"  id="note" placeholder="Case Description..."></textarea>
                                </div>
                                <div class="submit-area">
                                    <button type="submit" class="theme-btn-s6"><i class="fi flaticon-like"></i> Submit It Now</button>
                                    <div id="loader">
                                        <i class="ti-reload"></i>
                                    </div>
                                </div>
                                <div class="clearfix error-handling-messages">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
        </div> <!-- end container -->
    </section>
    <!-- end contact-section -->


    <!--  start contact-map -->
    <section class="contact-map-section">
        <h2 class="hidden">Contact map</h2>
        <div class="contact-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.068281686978!2d106.84868895092572!3d-6.254734795450438!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3b255bce217%3A0x7491bf2b2f7e6d94!2sKementerian%20Desa%2C%20Pembangunan%20Daerah%20Tertinggal%20dan%20Transmigrasi!5e0!3m2!1sen!2sid!4v1639062528044!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </section>
    <br>
<?= $this->endSection() ?>