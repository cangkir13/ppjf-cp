<?= $this->extend('layout/index') ?>

<?= $this->section('content_page') ?>

<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-12">
            <?php if (!empty(session()->getFlashdata('success'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 ">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="mb-0">Content</h3>
                        </div>
                        <div class="col-lg-6 col-5 text-right">
                            <a href="<?= base_url($route) ?>/create" class="btn btn-sm btn-primary">Create Konten</a>
                        </div>
                    </div>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Link</th>
                                <th scope="col">Published</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            <?php $i = 1;
                            foreach ($produk as $key => $value) : ?>
                                <tr>
                                    <td> <?= $i++ ?> </td>
                                    <td> <?= substr($value['title'], 0, 30)  ?> </td>
                                    <td> <?= substr($value['link'], 0, 30) ?> </td>
                                    <td>
                                        <?php if ($value['status'] == '1') {
                                            echo '<div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">';
                                            echo "<span class='ni ni-check-bold'></span></div>";
                                        } else {
                                            echo '<div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">';
                                            echo "<i class='far fa-window-close'></i></div>";
                                        } ?>
                                    </td>
                                    
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="<?= base_url($route) . "/" . $value['id'] ?>/edit" class="btn btn-sm btn-default text-white" data-toggle="tooltip" data-original-title="edit">
                                                <span class="fa fa-pencil-alt"></span>
                                            </a>
                                            
                                            <button type="button" class="btn btn-sm btn-danger deleteKOnten" data-toggle="modal" data-target="#deleteKOnten" data-delete="<?=$value['id']?>">
                                                <span class="fa fa-trash"></span>
                                            </button>

                                            <a href="<?=$route."/updateStatus/".$value['id']?>" class="btn btn-sm btn-primary text-white" data-toggle="tooltip" data-original-title="update status publish">
                                                <span class="fa fa-expand"></span>
                                            </a>
                                            
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- modal begin -->
        
        <!-- DELETE DATA -->
        <div class="col-12">
            <!-- modal detail -->
            <div class="modal fade" id="deleteKOnten" tabindex="-1" role="dialog" aria-labelledby="deleteKOnten" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title" id="modal-title-default">Ingin hapus konten?</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body body-delete">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p >
                                        Judul : <b id="judul"></b>
                                    </p>
                                </div>
                                <div class="col-sm-6" >
                                    <p>
                                        Link <b id="link"></b>
                                    </p>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        <div class="modal-footer">
                            <form action="<?= base_url($route) ?>/delete" method="post">
                                <?= csrf_field() ?>
                                <input type="hidden" name="id" id="id_value" readonly>
                                <button type="sumbit" class="btn btn-danger">Delete konten</button>
                            </form>
                            <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal edit -->
        </div>
    </div>
</div>

<?= $this->endSection() ?>


<?= $this->section('renderjs') ?>
    <?= $this->include('produk/event') ?>
<?= $this->endSection() ?>