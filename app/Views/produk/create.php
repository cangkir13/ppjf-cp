<?= $this->extend('layout/index') ?>

<?= $this->section('content_page') ?>
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <h4>Periksa Entrian Form</h4>
                    </hr />
                    <?php echo session()->getFlashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><?=$title_page?> </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <form action="<?= base_url($route) ?>" method="post" enctype="multipart/form-data">
                        <?= csrf_field() ?>
                        <h6 class="heading-small text-muted mb-4">Produk Hukum</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-title">Judul</label>
                                        <input name="title" type="text" id="input-title" class="form-control " placeholder="PPJF" value="<?= old('title') ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-author">Link</label>
                                        <input name="link" type="text" id="input-author" class="form-control " placeholder="https://jdih.kemendesa.go.id/" value="<?= old('link') ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Status</label>
                                        <select name="status" class="form-control" id="exampleFormControlSelect2">
                                            <option value="0">Draft</option>
                                            <option value="1">Published</option>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>