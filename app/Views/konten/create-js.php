
<script>
    $(document).on("click", '.showDetail', function(e) {
        
        let dataid = $(this).attr('data-user')
        $.ajax({
            url : `<?=$route?>/${dataid}/get`,
            method: 'GET',
            success : function(result) {
                console.log(result);
                $(".modal-body #judul").text(result.title)
                $(".modal-body #tag").text(result.tags)
                $(".modal-body #penulis").text(result.author)
                $(".modal-body #isi").text(result.content)
                let img = "<?=base_url()?>/"+result.image
                $("#gambar").attr('src', img)
            }
        })
    })

    $(document).on("click", '.deleteKOnten', function(e) {
        
        let dataid = $(this).attr('data-delete')
        $.ajax({
            url : `<?=$route?>/${dataid}/get`,
            method: 'GET',
            success : function(result) {
                $(".modal-footer #id_value").val(result.id)
                $(".body-delete #judul").text(result.title)
                $(".body-delete #tag").text(result.tags)
                $(".body-delete #penulis").text(result.author)
                $(".body-delete #isi").text(result.content.substr(0, 200, "..."))
                let img = "<?=base_url()?>/"+result.image
                $("#gambar_delete").attr('src', img);
            }
        })
    })
</script>