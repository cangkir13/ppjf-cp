<?= $this->extend('layout/index') ?>

<?= $this->section('content_page') ?>
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <h4>Periksa Entrian Form</h4>
                    </hr />
                    <?php echo session()->getFlashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><?=$title_page?> </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <form action="<?= base_url($route) ?>" method="post" enctype="multipart/form-data">
                        <?= csrf_field() ?>
                        <h6 class="heading-small text-muted mb-4">Konten baru</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-author">Penulis</label>
                                        <input name="author" type="text" id="input-author" class="form-control " placeholder="Sanca" value="<?= old('author') ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-title">Judul</label>
                                        <input name="title" type="text" id="input-title" class="form-control " placeholder="jessexample.com" value="<?= old('title') ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Tags</label>
                                        <input name="tags" type="text" id="input-first-name" class="form-control " placeholder="Headline kami" value="<?= old('tags') ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-kategori_id">Kategori Konten</label>
                                        <select name="kategori_id" class="form-control" id="exampleFormControlSelect2">
                                            <?php foreach ($kategoris as $key => $value) : ?>
                                                <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Isi konten</label>
                                        <textarea name="content" id="mytextarea" rows="30" class="form-control"><?= old('content') ?></textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Upload Gambar</label>
                                        <input name="image" type="file" id="input-first-name" class="form-control ">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Upload Gambar</label>
                                        <select name="status" class="form-control" id="exampleFormControlSelect2">
                                            <option value="draft">Draft</option>
                                            <option value="published">Published</option>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('renderjs')?>
    <script src="<?=base_url()?>/assets/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
      tinymce.init({
        selector: 'textarea'
      });
    </script>
<?= $this->endSection()?>