<?= $this->extend('layout/dashboard') ?>
<div class="navbar-inner">
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
            <?php $uri = uri_string(); ?>
            <li class="nav-item">
                <a class="nav-link <?= (uri_string() == 'admin') ? 'active' : '' ?> " href="<?= base_url() ?>/admin">
                    <i class="ni ni-tv-2 text-primary"></i>
                    <span class="nav-link-text">Dashboard </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= preg_match("/users/i", $uri)  ? 'active' : '' ?>" href="<?= base_url() ?>/admin/users">
                    <i class="ni ni-circle-08 text-orange"></i>
                    <span class="nav-link-text">Users</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= preg_match("/konten/i", $uri)  ? 'active' : '' ?>" href="<?= base_url() ?>/admin/konten">
                    <i class="ni ni-archive-2 text-primary"></i>
                    <span class="nav-link-text">Konten</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= preg_match("/youtube/i", $uri)  ? 'active' : '' ?>" href="<?= base_url() ?>/admin/youtube">
                    <i class="ni ni-tv-2 text-red"></i>
                    <span class="nav-link-text">Youtube</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= preg_match("/produk/i", $uri)  ? 'active' : '' ?>" href="<?= base_url() ?>/admin/produk">
                    <i class="ni ni-book-bookmark "></i>
                    <span class="nav-link-text">Produk Hukum</span>
                </a>
            </li>
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->

    </div>
</div>