<?= $this->extend('layout/dashboard') ?>
<?= $this->section('navbar') ?>
    <?= $this->include('layout/navbar') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="header bg-primary">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <p class="h2 text-white d-inline-block mb-0"><?=isset($title) ? $title: "John" ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->renderSection('content_page') ?>
<?= $this->endSection() ?>