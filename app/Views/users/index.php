<?= $this->extend('layout/index') ?>

<?= $this->section('content_page') ?>

<div class="container-fluid mt-2">
    <div class="row">
        <div class="col">
            <?php if (!empty(session()->getFlashdata('success'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 ">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="mb-0">Table Users</h3>
                        </div>
                        <div class="col-lg-6 col-5 text-right">
                            <a href="<?= base_url($route) ?>/create" class="btn btn-sm btn-primary">Create user</a>
                        </div>
                    </div>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Fullname</th>
                                <th scope="col">Email</th>
                                <th scope="col">Username</th>
                                <th scope="col">status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            <?php $i = 1;
                            foreach ($users as $key => $value) : ?>
                                <tr>
                                    <td> <?= $i++ ?> </td>
                                    <td> <?= $value['fullname'] ?> </td>
                                    <td> <?= $value['email'] ?> </td>
                                    <td> <?= $value['username'] ?> </td>
                                    <td>
                                        <?php if ($value['status'] == 1) {
                                            echo '<div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">';
                                            echo "<span class='ni ni-check-bold'></span></div>";
                                        } else {
                                            echo '<div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">';
                                            echo "<i class='far fa-window-close'></i></div>";
                                        } ?>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <!-- edit data -->
                                            <a href="<?= base_url($route) . "/" . $value['id'] ?>/edit" class="btn btn-sm btn-default text-white" data-toggle="tooltip" data-original-title="edit">
                                                <span class="fa fa-pencil-alt"></span>
                                            </a>
                                            <!-- show data -->
                                            <button type="button" class="btn btn-sm btn-primary text-white showDetail" data-toggle="modal" data-target="#showDetail" data-user="<?=$value['id']?>">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                            <!-- delete data -->
                                            <button type="button" class="btn btn-sm btn-danger text-white deleteData" data-toggle="modal" data-target="#deleteData" data-delete="<?=$value['id']?>">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- modal begin -->
        <!-- show data -->
        <div class="col-12">
            <!-- modal detail -->
            <div class="modal fade" id="showDetail" tabindex="-1" role="dialog" aria-labelledby="showDetail" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title" id="modal-title-default">Detail Users</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                            <p >
                                Email : <b id="email"></b>
                            </p>
                            <p>
                                Fullname : <b id="fullname"></b>
                            </p>
                            <p>
                                Username : <b id="username"></b>
                            </p>
                        </div>
                        
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal edit -->
        </div>

        <!-- delete user -->
        <div class="col-12">
            <!-- modal detail -->
            <div class="modal fade" id="deleteData" tabindex="-1" role="dialog" aria-labelledby="showDetail" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title" id="modal-title-default">Delete Users</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            
                        </div>
                        <div class="modal-body delete">
                            <h2>Apakah anda yakin, ingin menghapus akun ini?</h2>
                            <p >
                                Email : <b id="email"></b>
                            </p>
                            <p>
                                Fullname : <b id="fullname"></b>
                            </p>
                            <p>
                                Username : <b id="username"></b>
                            </p>
                        </div>
                        
                        <div class="modal-footer">
                            <form action="<?= base_url($route) ?>/delete" method="post">
                                <?= csrf_field() ?>
                                <input type="hidden" name="id" id="id_value">
                                <button type="sumbit" class="btn btn-danger">Delete user</button>
                            </form>
                            <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal edit -->
        </div>
        
    </div>
</div>


<?= $this->endSection() ?>

<?= $this->section('renderjs') ?>
    <?= $this->include('users/modals') ?>
<?= $this->endSection() ?>