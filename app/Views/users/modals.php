<script>
    $(document).on("click", '.showDetail', function(e) {
        console.log('edit');
        let dataid = $(this).attr('data-user')
        // $('#showDetail').attr('data-user', data);
        $.ajax({
            url : `<?=$route?>/${dataid}`,
            method: 'GET',
            success : function(result) {
                // console.log(result);
                $(".modal-body #email").text(result.email)
                $(".modal-body #fullname").text(result.fullname)
                $(".modal-body #username").text(result.username)
                
            }
        })
    });

    $(document).on("click", '.deleteData', function(e) {
        console.log('delete');
        let dataid = $(this).attr('data-delete')
        // $('#showDetail').attr('data-user', data);
        $.ajax({
            url : `<?=$route?>/${dataid}`,
            method: 'GET',
            success : function(result) {
                // console.log(result);
                $(".modal-footer #id_value").val(result.id)
                $(".modal-body #email").text(result.email)
                $(".modal-body #fullname").text(result.fullname)
                $(".modal-body #username").text(result.username)
                
            }
        })
    });
</script>