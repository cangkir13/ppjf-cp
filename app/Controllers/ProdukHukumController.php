<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProdukHukum;

class ProdukHukumController extends BaseController
{
    protected $modelsProduk;

    public function __construct()
    {
        $this->modelsProduk = new ProdukHukum();
        $this->title_page = "Produk Hukum";
        $this->title = 'Produk Hukum';
        $this->route = '/admin/produk';
        $this->page = 'produk';
    }
    
    public function index()
    {
        $produk = $this->modelsProduk->orderBy('id', 'DESC')->findAll();
        
        $data = [
            'title_page' => 'Data ' . $this->title_page,
            'title' => $this->title,
            'produk' => $produk,
            'route' => $this->route,
        ];

        return view($this->page.'/index', $data);
    }

    public function create()
    {
        $data = [
            'title_page' => "Tambah ". $this->title_page,
            'title' => 'Create ' . $this->title,
            'route' => $this->route . '/store'
        ];
        return view($this->page.'/create',  $data);
    }

    public function store()
    {
        $validate = $this->validate([
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul harus di isi'
                ]
                ],
            'link' => [
                'rules' => 'required|valid_url',
                'errors' => [
                    'required' => 'link harus diisi ',
                    'valid_url' => 'link harus sesuai'
                ]
            ]
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } 
        
        $title = $this->request->getVar('title');
        $link = $this->request->getVar('link');
        $status = $this->request->getVar('status');

        $insert = [
            'title' => $title,
            'link' => $link,
            'status' => $status,
        ];

        $this->modelsProduk->save($insert);
        
        session()->setFlashdata('success', "Data hasbeen created");
        return redirect()->to($this->route);   
    }

    public function edit($id){
        $findkontent = $this->modelsProduk->find($id);
        if(!$findkontent) {
            session()->setFlashdata('error', "Produk tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $data = [
                'title_page' => " Update ". $this->title_page,
                'title' => 'Update ' . $this->title,
                'route' => $this->route . '/update',
                'konten' => $findkontent,
            ];
            return view($this->page."/edit", $data);
        }
    }

    public function update()
    {
        $validate = $this->validate([
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul harus di isi'
                ]
                ],
            'link' => [
                'rules' => 'required|valid_url',
                'errors' => [
                    'required' => 'link harus diisi ',
                    'valid_url' => 'link harus sesuai'
                ]
            ]
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } 

        $request = $this->request;
        $id = $this->request->getVar('id');
        $title = $this->request->getVar('title');
        $link = $this->request->getVar('link');
        $status = $this->request->getVar('status');

        $update = [
            'title' => $title,
            'link' => $link,
            'status' => $status,
        ];
        
        $this->modelsProduk->update($id, $update);
        
        session()->setFlashdata('success', "Data hasbeen updated");
        return redirect()->to($this->route);
    }

    public function getOneDetail($id=null)
    {
        $data = $this->modelsProduk->find($id);
        if (!$data) {
            return $this->response->setJSON([
                'status' => false,
                'message' => 'not found'
            ]);
        }
        return $this->response->setJSON($data);
    }

    public function updatestatus($where = null)
    {
        $konten = $this->modelsProduk->find($where);
        if(!$konten){
            session()->setFlashdata('error', "Produk tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $status = ($konten['status'] == 1) ? 0 : 1;
            $this->modelsProduk->set('status', $status);
            $this->modelsProduk->set('updated_by', $this->users->user_id);
            $this->modelsProduk->where('id', $where);
            $this->modelsProduk->update();
            session()->setFlashdata('success', "Konten telah di update");
            return redirect()->to($this->route);
        }
    }

    public function delete()
    {
        $id = $this->request->getVar('id');
        $datauser = $this->modelsProduk->find($id);
        if ($datauser) {
            // dd($datauser);
            $this->modelsProduk->delete($id);
            session()->setFlashdata('success', "Data hasbeen deleted");
            return redirect()->to($this->route);
        } else {
            session()->setFlashdata('error', "ID not found");
            return redirect()->to($this->route);
        }
    }

}
