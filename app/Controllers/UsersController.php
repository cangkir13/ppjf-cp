<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Users;

class UsersController extends BaseController
{
    protected $modeluser;
    
    public function __construct(){
        $this->modeluser = new Users();
        $this->title_page = "Users";
        $this->title = 'Users';
        $this->route = '/admin/users';
    }

    public function index()
    {
        $data = [
            'title_page' => 'Data '. $this->title_page,
            'title' => $this->title ,
            'users' => $this->modeluser->findAll(),
            'route' => $this->route ,
            
        ];
        
        return view('users/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Create '. $this->title,
            'route' => $this->route. '/store'
        ];
        return view('users/create', $data);
    }

    public function store()
    {
        $validate = $this->validate([
            'email' => "required|is_unique[users.email]",
            'username' => 'required|min_length[6]',
            'fullname' => 'required|min_length[5]',
            'password' => 'required|min_length[8]',
            'password_confirm' => 'required|min_length[8]|matches[password]',
        ]);
        
        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } else {
            $request = $this->request;
            $insert = $this->modeluser->save([
                'email' => $request->getVar('email'),
                'username' => $request->getVar('username'),
                'fullname' => $request->getVar('fullname'),
                'password' => password_hash($request->getVar('password'), PASSWORD_BCRYPT) ,
                'created_by' => $this->users->user_id,
            ]);
            session()->setFlashdata('success', "Data hasbeen created");
            return redirect()->to($this->route);
        }
    }

    public function show($id= null)
    {
        $data = $this->modeluser->find($id);
        if (!$data) {
            return $this->response->setJSON([
                'status' => false,
                'message' => 'not found'
            ]);
        }
        return $this->response->setJSON($data);
    }

    public function edit($id=null) {
        $findUser = $this->modeluser->find($id);
        if(!$findUser){
            session()->setFlashdata('error', "User not found");
            return redirect()->back();
        } else {
            $data = [
                'title_page' => " Update ". $this->title_page,
                'title' => 'Update ' . $this->title,
                'route' => $this->route . '/update',
                'users' => $findUser,
            ];
            return view('users/edit', $data);
        }
    }

    public function update()
    {
        $validate = $this->validate([
            'username' => 'required|min_length[6]',
            'fullname' => 'required|min_length[5]',
            'password' => 'required|min_length[8]',
            'password_confirm' => 'required|min_length[8]|matches[password]',
        ]);

        if (!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } else {
            $request = $this->request;
            $this->modeluser->where('id', $request->getVar('id'));
            $updated = [
                'username' => $request->getVar('username'),
                'fullname' => $request->getVar('fullname'),
                'password' => password_hash($request->getVar('password'), PASSWORD_BCRYPT),
                'updated_by' => $this->users->user_id,
            ];

            $this->modeluser->update($request->getVar('id'), $updated);
            session()->setFlashdata('success', "Data hasbeen updated");
            return redirect()->to($this->route);
        }
    }

    public function delete()
    {
        $id = $this->request->getVar('id');
        
        $datauser = $this->modeluser->find($id);
        if ($datauser) {
            $this->modeluser->delete($id);
            session()->setFlashdata('success', "Data hasbeen deleted");
            return redirect()->to($this->route);
        } else {
            session()->setFlashdata('error', "ID not found");
            return redirect()->to($this->route);
        }
    }
}