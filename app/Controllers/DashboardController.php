<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class DashboardController extends BaseController
{
    public function index()
    {
        $data = [
            'title_page' => "Dashboard",
            'title' => 'Welcome ' .$this->users->username,
        ];
        return view('layout/index', $data);
    }
}
