<?php

namespace App\Controllers;
use App\Models\Konten;
use App\Models\Kategori;
use App\Models\Users;
use App\Models\Youtube;
use App\Models\ProdukHukum;

class Home extends BaseController
{
    protected $modelsuser;
    protected $modelsKontent;
    protected $modelsKategori;
    protected $modelyoutube;
    protected $modelproduk;

    public function __construct() {
        $this->modeluser = new Users();
        $this->modelsKategori = new Kategori();
        $this->modelsKontent = new Konten();
        $this->modelyoutube = new Youtube();
        $this->modelproduk = new ProdukHukum();
    }

    public function index()
    {
     
        $headline = $this->modelsKontent->where(['kategori_id' => 1, 'status' => 'published'])->orderBy('created_at', 'DESC')->findAll(3);
        $content = $this->modelsKontent->where(['kategori_id' => 2, 'status' => 'published'])->orderBy('created_at', 'DESC')->findAll();
        $youtube = $this->modelyoutube->where('status', 1)->orderBy('id', 'DESC')-> findAll(6);
        // dd($headline);
        $data = [
            'title_page' => "Dashboard",
            'title' => 'PPJF KEMENDESA ',
            'headline' => $headline,
            'content' => $content,
            'youtube' => $youtube
        ];
        return view('pages/home', $data);
    }
    
    public function contactus()
    {
        $data = [
            'title_page' => "Kontak kami",
            'title' => 'Kontak kami'
        ];
        return view('pages/contact', $data);
    }

    public function chatboot()
    {
        $from = $this->request->getVar('from');
        $body = $this->request->getVar('body');
        $client = new \GuzzleHttp\Client();

        try {
            //code...
        } catch (\Throwable $th) {
            $response = json_decode($th->getResponse()->getBody());
            $this->sendWhatsAppMessage($response->message, $from);
        }
    }

    public function sendWhatsAppMessage(string $message, string $recipient)
    {
        $twilio_whatsapp_number = getenv('TWILIO_WHATSAPP_NUMBER');
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");

        $client = new Client($account_sid, $auth_token);
        return $client->messages->create($recipient, array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));
    }

    public function article($where = null)
    {
        $article = $this->modelsKontent->where(['title' => $where])->first();

        if (!$article) {
            return view('pages/404');
        }

        $kategori = $this->modelsKontent->select(
            'count(kontens.kategori_id) as jml, kategoris.name'
        )
        ->join('kategoris', 'kontens.kategori_id = kategoris.id')
        ->groupBy('kontens.kategori_id')->findAll();
        // dd($kategori);
        $data = [
            'title_page' => $article['content'],
            'title' => 'Artikel '. $article['title'],
            'author' => $article['author'],
            'article' => $article,
            // 'event' => $data,
            'categoris' => $kategori
        ];        

        return view('pages/detail', $data);
    }

    public function categori_article($where= null)
    {
        $category = $this->modelsKategori->where('name', $where)->first();
        if (!$category) {
            return view('pages/404');
        }

        $articles = $this->modelsKontent
            ->where(
                ['kategori_id' => $category['id'], 
                'status' => "published"
            ])
            ->orderBy('id', 'DESC')
            ->findAll(5);
        
        $data = [
            'title_page' => $category['name'],
            'title' => 'Catogory '. $category['name'],
            'articles' => $articles
        ]; 
        return view('pages/category', $data);
    }
    
    public function informasi()
    {
        $article = $this->modelsKontent->where('status', 'published')->orderBy('id', 'DESC')->findAll(3);
        $youtube = $this->modelyoutube->where('status', 1)->orderBy('id', 'DESC')-> first();
        $produks = $this->modelproduk->where('status', 1)->orderBy('id', 'DESC')->findAll(10);

        $data = [
            'title_page' => "Pusat data dan Informasi",
            'title' => "Pusat data dan Informasi",
            'articles' => $article,
            'youtube' => $youtube,
            'produks' => $produks
        ];
        return view('pages/informasi', $data);
    }

    public function Profile()
    {
        $data = [
            'title_page' => "Profile PPJF KEMENDESA",
            'title' => "Profile PPJF KEMENDESA",
            'author' => "PPJF KEMENDESA"
        ];
        return view('pages/profile', $data);
    }

    public function Layanan()
    {
        $data = [
            'title_page' => "Layanan PPJF KEMENDESA",
            'title' => "Layanan PPJF KEMENDESA",
            'author' => "PPJF KEMENDESA"
        ];
        return view('pages/layanan', $data);
    }
}
