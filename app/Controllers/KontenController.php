<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Konten;
use App\Models\Kategori;
use App\Models\Users;

class KontenController extends BaseController
{

    protected $modelsuser;
    protected $modelsKontent;
    protected $modelsKategori;

    public function __construct()
    {
        $this->modeluser = new Users();
        $this->modelsKategori = new Kategori();
        $this->modelsKontent = new Konten();
        $this->title_page = "Konten";
        $this->title = 'Konten';
        $this->route = '/admin/konten';
        $this->page = 'konten';
    }
    
    public function index()
    {

        $content = $this->modelsKontent
        ->select('kontens.id, kontens.title, kontens.image, kontens.content, kontens.author, kontens.status, kontens.created_at, k.name')
        ->join('kategoris as k', 'k.id = kontens.kategori_id')
        ->orderBy('kontens.created_at', 'DESC')->findAll();
        
        $data = [
            'title_page' => 'Data ' . $this->title_page,
            'title' => $this->title,
            'konten' => $content,
            'route' => $this->route,
        ];

        return view($this->page.'/index', $data);
    }

    public function create() {
        $data = [
            'title_page' => "Tambah ". $this->title_page,
            'kategoris' => $this->modelsKategori->findAll(),
            'title' => 'Create ' . $this->title,
            'route' => $this->route . '/store'
        ];
        return view('konten/create', $data);
    }

    public function store()
    {
        
        // dd($this->request->getVar());
        $validate = $this->validate([
            'author' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Nama Penulis harus di isi'
                ]
                ],
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'judul harus diisi '
                ]
                ],
            'image' => [
                'rules' => 'uploaded[image]|max_size[image,2024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'Pilih gambar terlebih dahulu',
                    'max_size' => 'Gambar terlalu besar',
                    'is_image' => 'File harus berupa gambar',
                    'mime_in' => 'File harus berupa gambar'
                ]
            ]
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } else {
            $title = $this->request->getVar('title');
            $author = $this->request->getVar('author');
            $tags = $this->request->getVar('tags');
            $kategori_id = $this->request->getVar('kategori_id');
            $content = $this->request->getVar('content');
            $status = $this->request->getVar('status');
            $fileimage = $this->request->getFile('image');   
            $imagename = $fileimage->getRandomName();
            $fileimage->move('images', $imagename);
            
            $this->modelsKontent->save([
                'title' => $title,
                'author' => $author,
                'tags' => $tags,
                'content' => $content,
                'image' => "images/".$imagename,
                'kategori_id' => $kategori_id,
                'status' => $status,
                'created_by' => $this->users->user_id,
            ]);

            session()->setFlashdata('success', "Data hasbeen created");
            return redirect()->to($this->route);
        }
    }

    public function edit($id){
        $findkontent = $this->modelsKontent->find($id);
        if(!$findkontent) {
            session()->setFlashdata('error', "Konten tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $data = [
                'title_page' => " Update ". $this->title_page,
                'title' => 'Update ' . $this->title,
                'route' => $this->route . '/update',
                'konten' => $findkontent,
                'kategoris' => $this->modelsKategori->findAll(),
            ];
            return view($this->page."/edit", $data);
        }
    }

    public function update()
    {
        
        $validate = $this->validate([
            'author' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Nama Penulis harus di isi'
                ]
                ],
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'judul harus diisi '
                ]
                ],
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } else {
            $id = $this->request->getVar('id');
            $title = $this->request->getVar('title');
            $author = $this->request->getVar('author');
            $tags = $this->request->getVar('tags');
            $kategori_id = $this->request->getVar('kategori_id');
            $content = $this->request->getVar('content');
            $status = $this->request->getVar('status');
            // $fileimage = $this->request->getFile('image');   
            // var_dump($fileimage);
            $update = [
                'title' => $title,
                'author' => $author,
                'tags' => $tags,
                'content' => $content,
                // 'image' => "images/".$imagename,
                'kategori_id' => $kategori_id,
                'status' => $status,
                'updated_by' => $this->users->user_id,
            ];

            if ($this->request->getFile('image')->isValid() ) {
                $fileimage = $this->request->getFile('image'); 
                
                $validateimg = $this->validate([
                    'image' => [
                        'rules' => 'max_size[image,2024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
                        'errors' => [
                            'uploaded' => 'Pilih gambar terlebih dahulu untuk update',
                            'max_size' => 'Gambar terlalu besar',
                            'is_image' => 'File harus berupa gambar',
                            'mime_in' => 'File harus berupa gambar'
                        ]
                    ]
                ]);
                if(!$validateimg) {
                    session()->setFlashdata('error', $this->validator->listErrors());
                    return redirect()->back()->withInput();
                } 

                $imagename = $fileimage->getRandomName();
                $fileimage->move('images', $imagename);
                $update['image'] = "images/".$imagename;
                
            } 
            
            $this->modelsKontent->update($id, $update);

            session()->setFlashdata('success', "Data hasbeen created");
            return redirect()->to($this->route);
        }
    }

    public function updatestatus($where=null)
    {
        $konten = $this->modelsKontent->find($where);
       
        if(!$konten){
            session()->setFlashdata('error', "Konten tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $status = ($konten['status'] === "published") ? "draft" : "published";
            $this->modelsKontent->set('status', $status);
            $this->modelsKontent->set('updated_by', $this->users->user_id);
            $this->modelsKontent->where('id', $where);
            $this->modelsKontent->update();
            session()->setFlashdata('success', "Konten telah di update");
            return redirect()->to($this->route);
        }
    }
    
    public function getOneDetail($id=null)
    {
        $data = $this->modelsKontent->find($id);
        if (!$data) {
            return $this->response->setJSON([
                'status' => false,
                'message' => 'not found'
            ]);
        }
        return $this->response->setJSON($data);
    }

    public function delete()
    {
        $id = $this->request->getVar('id');
        $datauser = $this->modelsKontent->find($id);
        if ($datauser) {
            // dd($datauser);
            $this->modelsKontent->delete($id);
            session()->setFlashdata('success', "Data hasbeen deleted");
            return redirect()->to($this->route);
        } else {
            session()->setFlashdata('error', "ID not found");
            return redirect()->to($this->route);
        }
    }
}
