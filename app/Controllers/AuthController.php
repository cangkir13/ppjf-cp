<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Users;
use Firebase\JWT\JWT;

class AuthController extends BaseController
{
    protected $modeluser;
    public function __construct()
    {
        $this->modeluser = new Users();
        $this->title = 'Auth';
        $this->route = '/auth';   
    }

    public function index()
    {
        $data = [
            'route' => $this->route
        ];
        return view('auth/login', $data);
    }

    public function authLogin()
    {
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $data = $this->modeluser->where('email', $email)->first();
        // dd($data);
        if($data) {
            $passuser = $data['password'];
            $authenticate = password_verify($password, $passuser);
            if ($authenticate) {
                $key = getenv('JW_SECRET');
                $payload =  [
                    'user' => [
                        'user_id' => $data['id'],
                        "iat" => 1356999524,
                        "nbf" => 1357000000,
                        'email' => $email,
                        'username' => $data['username']
                    ],
                ];
                $token = JWT::encode($payload['user'], $key);
                $datasession = [
                    'isLogged' => true,
                    'token' => $token
                ];
                // $decode = JWT::decode($token, $key, ['HS256']);
                // dd($token, $decode);
                session()->set($datasession);
                return redirect()->to('/admin');
            } else {
                session()->setFlashdata('error', "Salah password");
                return redirect()->back()->withInput();
            }
        } else {
            session()->setFlashdata('error', "Email tidak terdaftar");
            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('/auth');
    }
}
