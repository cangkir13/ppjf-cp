<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Youtube;

class YoutubeController extends BaseController
{
    protected $modelyoutube;

    public function __construct()
    {
        $this->modelyoutube = new Youtube();
        $this->title_page = "Youtube Chanel";
        $this->title = 'Youtube Chanel';
        $this->route = '/admin/youtube';
        $this->page = 'youtube';
    }

    public function index()
    {
        $youtube = $this->modelyoutube->orderBy('id', 'DESC')-> findAll();

        $data = [
            'konten' => $youtube,
            'title_page' => 'Data ' . $this->title_page,
            'title' => $this->title,
            'route' => $this->route,
        ];

        return view($this->page.'/index', $data);

    }

    public function create()
    {
        $data = [
            'title_page' => "Tambah ". $this->title_page,
            'title' => 'Create ' . $this->title,
            'route' => $this->route . '/store'
        ];
        return view($this->page.'/create',  $data);
    }

    public function store()
    {
        $validate = $this->validate([
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul harus di isi'
                ]
                ],
            'content' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'konten harus diisi '
                ]
                ],
            'link' => [
                'rules' => 'required|valid_url',
                'errors' => [
                    'required' => 'link harus diisi ',
                    'valid_url' => 'link harus sesuai'
                ]
            ]
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } 

        $request = $this->request;
        if (!preg_match('/https:\/\/www\.youtube\.com\/watch\?v=[^&]+/', $request->getVar('link'))) {    
            session()->setFlashdata('error', "Link youtube tidak valid");
            return redirect()->back()->withInput();
        } else {
            $title = $this->request->getVar('title');
            $link = $this->request->getVar('link');
            $content = $this->request->getVar('content');
            $status = $this->request->getVar('status');

            $insert = [
                'title' => $title,
                'link' => str_replace("watch?v=", "embed/" , $link),
                'content' => $content,
                'status' => $status,
                'created_by' => $this->users->user_id,
            ];
            $this->modelyoutube->save($insert);
            
            session()->setFlashdata('success', "Data hasbeen created");
            return redirect()->to($this->route);
        }
        
    }

    public function edit($id){
        $findkontent = $this->modelyoutube->find($id);
        if(!$findkontent) {
            session()->setFlashdata('error', "Konten tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $data = [
                'title_page' => " Update ". $this->title_page,
                'title' => 'Update ' . $this->title,
                'route' => $this->route . '/update',
                'konten' => $findkontent,
            ];
            return view($this->page."/edit", $data);
        }
    }

    public function update()
    {
        $validate = $this->validate([
            'title' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul harus di isi'
                ]
                ],
            'content' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'konten harus diisi '
                ]
                ],
            'link' => [
                'rules' => 'required|valid_url',
                'errors' => [
                    'required' => 'link harus diisi ',
                    'valid_url' => 'link harus sesuai'
                ]
            ]
        ]);

        if(!$validate) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        } 

        $request = $this->request;
        if (!preg_match('/https:\/\/www\.youtube\.com\/watch\?v=[^&]+/', $request->getVar('link'))) {    
            session()->setFlashdata('error', "Link youtube tidak valid");
            return redirect()->back()->withInput();
        } else {
            $id = $this->request->getVar('id');
            $title = $this->request->getVar('title');
            $link = $this->request->getVar('link');
            $content = $this->request->getVar('content');
            $status = $this->request->getVar('status');

            $update = [
                'title' => $title,
                'link' => str_replace("watch?v=", "embed/" , $link),
                'content' => $content,
                'status' => $status,
                'updated_by' => $this->users->user_id,
            ];
            // dd($update);
            $this->modelyoutube->update($id, $update);
            
            session()->setFlashdata('success', "Data hasbeen updated");
            return redirect()->to($this->route);
        }
    }

    public function getOneDetail($id=null)
    {
        $data = $this->modelyoutube->find($id);
        if (!$data) {
            return $this->response->setJSON([
                'status' => false,
                'message' => 'not found'
            ]);
        }
        return $this->response->setJSON($data);
    }

    public function updatestatus($where = null)
    {
        $konten = $this->modelyoutube->find($where);
        if(!$konten){
            session()->setFlashdata('error', "Konten tidak ditemukan");
            return redirect()->to($this->route);
        } else {
            $status = ($konten['status'] == 1) ? 0 : 1;
            $this->modelyoutube->set('status', $status);
            $this->modelyoutube->set('updated_by', $this->users->user_id);
            $this->modelyoutube->where('id', $where);
            $this->modelyoutube->update();
            session()->setFlashdata('success', "Konten telah di update");
            return redirect()->to($this->route);
        }
    }
    
    public function delete()
    {
        $id = $this->request->getVar('id');
        $datauser = $this->modelyoutube->find($id);
        if ($datauser) {
            // dd($datauser);
            $this->modelyoutube->delete($id);
            session()->setFlashdata('success', "Data hasbeen deleted");
            return redirect()->to($this->route);
        } else {
            session()->setFlashdata('error', "ID not found");
            return redirect()->to($this->route);
        }
    }

}
