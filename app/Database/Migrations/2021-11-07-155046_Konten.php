<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Konten extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'tags' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'author' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'content' => [
                'type' => 'TEXT',
                'null' => false
            ],
            'image' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'kategori_id' => [
                'type' => 'INT',
                'null' => false
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' => ['published', 'draft'],
                'default' => 'draft'
            ],
            'created_by' => [
                'type' => 'INT',
                'null' => true
            ],
            'updated_by' => [
                'type' => 'INT',
                'null' => true
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->forge->addKey('id', TRUE);

        $this->forge->createTable('kontens', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('kontens');
    }
}
