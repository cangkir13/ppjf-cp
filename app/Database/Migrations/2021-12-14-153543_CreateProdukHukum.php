<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateProdukHukum extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'link' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'status' => [
                'type' => 'Boolean',
                'default' => 1
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->forge->addKey('id', TRUE);

        $this->forge->createTable('produk_hukums', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('produk_hukums');
    }
}
