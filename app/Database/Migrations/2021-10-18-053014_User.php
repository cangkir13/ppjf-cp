<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'unsigned'       => true,
                'auto_increment' => true
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'fullname' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint'     => '255'
            ],
            'status' => [
                'type' => 'TINYINT',
                'default' => 1
            ],
            'created_by' => [
                'type' => 'INT',
                'null' => true
            ],
            'updated_by' => [
                'type' => 'INT',
                'null' => true
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->forge->addKey('id', TRUE);

        $this->forge->createTable('users', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
