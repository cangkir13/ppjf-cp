<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Kategori extends Seeder
{
    public function run()
    {
        $data = [
            array('name' => 'HeadLine'),
            array('name' => 'Kontens'),
            array('name' => 'News')
        ];

        foreach ($data as $key => $value) {
            echo $value['name'];
        }

        $this->db->table('kategoris')->insertBatch($data);
    }
}
