<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeed extends Seeder
{
    public function run()
    {
        $data = [];
        for($i =0; $i < 5; $i++){
            $insert = [
                'fullname' => static::faker()->name,
                'email' => static::faker()->email,
                'username' => static::faker()->userName,
                'password' => password_hash("password", PASSWORD_BCRYPT),
            ];
            echo json_encode($insert['email']) . "\n"; 
            $data[] = $insert;
        }

        $this->db->table('users')->insertBatch($data);
    }
}
