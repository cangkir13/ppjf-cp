<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

$routes->get('Home', 'Home');
$routes->get('Article/(:any)', 'Home::article/$1');
$routes->get('categories/(:any)', 'Home::categori_article/$1');
$routes->get('contact', 'Home::contactus');
$routes->get('profile', 'Home::Profile');
$routes->get('pusat-data', 'Home::informasi');
$routes->get('layanan', 'Home::Layanan');
$routes->get('notfound', function() {
    return view('pages/404');
});

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->group('/auth', function($routes) {
    $routes->get('/', 'AuthController::index');
    $routes->post('authlogin', 'AuthController::authLogin');
    $routes->get('logout', 'AuthController::logout');
});

$routes->group('/admin', ['filter' => 'auth'], function($routes) {
    $routes->get('/', 'DashboardController::index');
    $routes->group('users', function($routes) {
        $routes->get('/', 'UsersController');
        $routes->get('(:num)', 'UsersController::show/$1');
        $routes->get('create', 'UsersController::create');
        $routes->post('store', 'UsersController::store');
        $routes->get('(:num)/edit', 'UsersController::edit/$1');
        $routes->post('update', 'UsersController::update');
        $routes->post('delete', 'UsersController::delete');
    });

    $routes->group('konten', function($routes) {
        $routes->get('/', 'KontenController::index/$1');
        $routes->get('create', 'KontenController::create', ['as' => 'konten.create']);
        $routes->get('updateStatus/(:num)', 'KontenController::updatestatus/$1', ['as' => 'konten.updatestatus']);
        $routes->get('(:num)/get', 'KontenController::getOneDetail/$1', ['as' => 'konten.get']);
        $routes->get('(:num)/edit', 'KontenController::edit/$1', ['as' => 'konten.edit']);
        $routes->post('store', 'KontenController::store');
        $routes->post('update', 'KontenController::update');
        $routes->post('delete', 'KontenController::delete');
    });

    $routes->group('youtube', function($routes) {
        $routes->get('/', 'YoutubeController::index');
        $routes->get('create', 'YoutubeController::create', ['as' => 'youtube.create']);
        $routes->post('store', 'YoutubeController::store', ['as' => 'youtube.store']);
        $routes->get('(:num)/get', 'YoutubeController::getOneDetail/$1', ['as' => 'youtube.get']);
        $routes->get('(:num)/edit', 'YoutubeController::edit/$1', ['as' => 'youtube.edit']);
        $routes->get('updateStatus/(:num)', 'YoutubeController::updatestatus/$1', ['as' => 'youtube.updatestatus']);
        $routes->post('update', 'YoutubeController::update');
        $routes->post('delete', 'YoutubeController::delete');
    });

    $routes->group('produk', function($routes) {
        $routes->get('/', 'ProdukHukumController::index');
        $routes->get('create', 'ProdukHukumController::create', ['as' => 'produk.create']);
        $routes->post('store', 'ProdukHukumController::store', ['as' => 'produk.store']);
        $routes->get('(:num)/get', 'ProdukHukumController::getOneDetail/$1', ['as' => 'produk.get']);
        $routes->get('(:num)/edit', 'ProdukHukumController::edit/$1', ['as' => 'produk.edit']);
        $routes->get('updateStatus/(:num)', 'ProdukHukumController::updatestatus/$1', ['as' => 'produk.updatestatus']);
        $routes->post('update', 'ProdukHukumController::update');
        $routes->post('delete', 'ProdukHukumController::delete');
    });
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
